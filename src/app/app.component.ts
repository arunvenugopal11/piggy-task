import { Component } from '@angular/core';
import { DataService } from './services/app.service';
import {Fund, mdata, mResponse, mFundDetails, mFund, MastermFund} from './model/mutualFunds'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [DataService]
})
export class AppComponent {
  title = 'app'; 

  res : mResponse;
  mData :  mdata;
  dataService: DataService;
  compare1Fund: mFund;
  compare2Fund:mFund;
  mastermFunds : Array<MastermFund>;
  selectedFund1: mFund;
  selectedFund2: mFund;


  // constructor(private MyService: MyService){

  // }
  constructor(public dataSer: DataService) {
    this.dataService = dataSer;
    this.dataService.GetAllMutualFunds().then(
      (res) => {
        this.mastermFunds = res;
      }
    )
    
  }

  
  compare1OnChange(selectedValue)
  {
    if(selectedValue == "-1")
    {
      this.selectedFund1 = null;
      return;
    }
      
    this.dataService.GetMutualFund(selectedValue).then(
      (resp) => {
        
        this.selectedFund1 = resp;
      }
    )
  }
  compare2OnChange(selectedValue)
  {
    if(selectedValue == "-1")
    {
      this.selectedFund2 = null;
      return;
    }
    this.dataService.GetMutualFund(selectedValue).then(
      (resp) => {
        
        this.selectedFund2 = resp;
      }
    )
  }
  CompareFunds()
  {
    this.compare1Fund = this.selectedFund1;
    this.compare2Fund = this.selectedFund2;
  }


}
