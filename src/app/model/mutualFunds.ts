export class MastermFund
{
    amc: string;
    riskometer: string;
    id : Number;
    name : string;
    sub_category : string;
    category: string;
    scheme_key : string;
}
export class Fund
{
    dividend_type : string;
    plan_type : string;
    key : string;
    launch_date : string;
    close_date : string;
}

export class mdata{
    funds : Array<Fund>;
    mutual_fund : mFund;
}
export class mFund{
    details : mFundDetails;
    nav : Number;
    nav_refresh_date : string;
    dividend_type : string;
}
export class mFundDetails {
    suitability : string;
    name : string;
    category : string;
    minimum_subscription : Number;
    id: Number;
    benchmark_text: string;
    riskometer : string;
    exit_load_text : string;
    scheme_type : string;
    scheme_class : string;
    minimum_addition_subscription : Number;
    
}
export class mResponse
{
    data : mdata;
}


