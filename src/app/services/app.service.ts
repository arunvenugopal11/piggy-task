 import { Injectable } from '@angular/core';
 import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Options } from 'selenium-webdriver/chrome';
import 'rxjs/add/operator/toPromise';
import {MastermFund, mFund} from '../model/mutualFunds'

@Injectable()
export class DataService {

    http : HttpClient;
    
    constructor (_http :HttpClient)
    {
        this.http = _http;
    }
    
    URLMutualFund : string = "https://api.piggy.co.in/v1/mf/?key=";
    URLAllMutualFund: string = "https://api.piggy.co.in/v2/mf/search/";
    GetMutualFund(key:string) : Promise<mFund>
    {
        let fheaders = new HttpHeaders();
        fheaders = fheaders.set('Authorization', 'Token a41d2b39e3b47412504509bb5a1b66498fb1f43a');
        fheaders = fheaders.set('Content-Type','application/json');
        fheaders = fheaders.set('Accept','application/json');
        
        return this.http.get(this.URLMutualFund+key,{headers:fheaders}).toPromise().then(
            (response) => {
                console.log(response);
                return JSON.parse(JSON.stringify(response)).data.mutual_fund as mFund;
            }
        )
        
    }

    GetAllMutualFunds() : Promise<Array<MastermFund>>
    {
        let fheaders = new HttpHeaders();
        fheaders = fheaders.set('Authorization', 'Token a41d2b39e3b47412504509bb5a1b66498fb1f43a');
       // fheaders = fheaders.set('Cache-Control','no-cache');
        fheaders = fheaders.set('Content-Type','application/json');
        fheaders = fheaders.set('Accept','application/json');
        return this.http.post(this.URLAllMutualFund,'{"rows":1500,"search":"","offset":0,"sort":"amc"}',{headers:fheaders}).toPromise().then(
            (res) => {
                return JSON.parse(JSON.stringify(res)).data.search_results as Array<MastermFund>;
            }
        )
    }

}